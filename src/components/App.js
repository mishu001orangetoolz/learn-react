import './App.css';
import Counter from "./Counter/Counter";
import React, {Component} from 'react'
import Row from "./Row/Row";
import Post from "./Post/Post";

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            rowArray: [
                { emailReadonly: 1, passwordReadonly: 1, editAttribute: "Make Editable"},
                { emailReadonly: 0, passwordReadonly: 0, editAttribute: "Make Readonly"}
            ]
        }
    }


    deleteRow = (index) => {
        let copyRowArray = Object.assign([], this.state.rowArray);
        delete copyRowArray[index];
        // copyRowArray.splice(index, 1);
        this.setState({
            rowArray: copyRowArray
        })
    }

    createNewRow = () => {
        let copyRowArray = Object.assign([], this.state.rowArray);
        copyRowArray.push({
            emailReadonly: 0,
            passwordReadonly: 0,
            editAttribute: "Make Readonly"
        })
        this.setState({
            rowArray: copyRowArray
        })

    }


    editAttributeAction = (index) => {
        const editAttribute = this.state.rowArray[index].editAttribute;
        let copyRowArray = Object.assign([], this.state.rowArray);
        if (editAttribute == "Make Editable") {
            copyRowArray[index].emailReadonly = 0;
            copyRowArray[index].passwordReadonly = 0;
            copyRowArray[index].editAttribute = "Make Readonly";
        } else {
            copyRowArray[index].emailReadonly = 1;
            copyRowArray[index].passwordReadonly = 1;
            copyRowArray[index].editAttribute = "Make Editable";
        }

        this.setState({
            rowArray: copyRowArray
        })
    }

    render() {
        return (
            <div className="App">
                <div className="container my-3">
                    {this.state.rowArray.map((row, index) => {
                        return <Row key={index}
                                    emailReadonly={row.emailReadonly}
                                    passwordReadonly={row.passwordReadonly}
                                    editAttribute={row.editAttribute}
                                    editAttributeAction={this.editAttributeAction.bind(this, index)}
                                    deleteRow={this.deleteRow.bind(this, index)}
                        />
                    })}
                    <button className="btn btn-primary" onClick={this.createNewRow}>Create New</button>
                </div>
            </div>
        )
    }
}

export default App;

