import React, {Component} from 'react';

class Post extends Component {
    render() {
        return (
            <div>
                <li>
                id = {this.props.id}
                <br/>
                body: {this.props.body}
                <br/>
                <button className="btn btn-danger" onClick={this.props.deletePost}>Delete</button>
                <hr/>
                </li>

            </div>
        );
    }
}

export default Post;