import React, {Component} from 'react';
import {bindReporter} from "web-vitals/dist/modules/lib/bindReporter";

class Row extends Component {
    render() {
        return (
            <div className="row">
                <div className="col-md-4">
                    <input type="text" name="email" placeholder="Enter your email"
                           readOnly={this.props.emailReadonly}/>

                </div>
                <div className="col-md-4">

                    <input type="text" name="password" placeholder="Enter your password"
                           readOnly={this.props.passwordReadonly}/>
                </div>
                <div className="col-md-2">
                    <button className="btn btn-danger" onClick={this.props.deleteRow}>Remove</button>
                </div>
                <div className="col-md-2">
                    <button className="btn btn-info"
                            onClick={this.props.editAttributeAction}>{this.props.editAttribute}</button>
                </div>
                <br/><br/>

            </div>

        )
    }
}

export default Row;